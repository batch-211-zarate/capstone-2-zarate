/*
User:

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Please enter a valid email address"]
	},
	password: {
		type: String,
		required: [true, "Please enter a password"]
	},
	mobileNo: {
		type: String,
		required: [true, "Please enter a mobile number"]
	},
	isAdmin: {
		type: Boolean,
		default: false,
	}

*/
/*

Products:

	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
    isActive: {
		type: Boolean,
		default: true
	},
	orders : [
		{
			orderId: {
				type: String,
				required: true
			},
			quantity: {
				type: Number,
				required: true
			}
		}
	],
},

{timestamps: true}

);

*/

/*

Orders: 

	userId: {
		type: String,
		required: true
	},
    products: [{
        productId: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            required: true,
            default: 1
        }
    }],
    totalAmount: {
        type: Number,
        required: true
    }
},

{timestamps: true}

);


*/