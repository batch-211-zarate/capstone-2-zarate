const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');

// Routes for creating a product (ADMIN ONLY).

router.post('/addProduct',  auth.verify, productController.addProduct);

// Routes for retrieving all Products.

router.get('/all', productController.getAllProduct);

// Routes for retrieving all Active Products.

router.get('/active', productController.getActiveProduct);

// Routes for retrieving single product.

router.get('/:id', productController.getProduct);

// Routes for Updating products details (ADMIN ONLY).

router.put('/update/:id', auth.verify, productController.updateProduct);

// Routes for Archiving products(ADMIN ONLY).

router.put('/archive/:id', auth.verify, productController.archiveProduct);

// Routes for Unarchive/Activate products(ADMIN ONLY).

router.put('/activate/:id', auth.verify, productController.unarchiveProduct);


module.exports = router;