const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const orderController = require('../controllers/orderControllers');
const productController = require('../controllers/productControllers');
const auth = require('../auth');



// Routes for Adding Products

router.post('/cart/add', auth.verify, orderController.addProductToCart)

// Routes for Updating quantity

router.put('/cart/update', auth.verify, orderController.updateQuantity)

// Routes for Removing Products from Cart

router.delete('/cart/delete', auth.verify, orderController.deleteProduct)

// Routes for checkout

router.post('/checkout', auth.verify, orderController.checkout)


module.exports = router;