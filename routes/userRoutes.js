const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');


// Route for Users Registration

router.post('/signup',  userController.registerUser);

// Routes for Users Authentication

router.post('/login', userController.loginUser);

// Routes for Set users as an Admin (Stretch Goal: ADMIN ONLY)

router.put('/details/:id', auth.verify, userController.changeUserStatus)

// Routes for Set Admin as a User (Stretch Goal: ADMIN ONLY) (Other Feature)

router.put('/admin/details/:id', auth.verify, userController.changeAdminStatus)

// Routes for Retrieving Users Details

router.get('/details', auth.verify, userController.getDetail)

// Routes for Retrieving All Users Details (ADMIN ONLY)
router.get('/details/all', auth.verify, userController.getAllUsers)

// Routes for Authenticated users to Create Orders 

router.post('/orders', auth.verify, userController.createOrders)

// Routes for Retrieving Authenticated Users Orders

router.get('/orders', auth.verify, userController.getOrders)

// Routes for Retrieving all Orders (ADMIN ONLY)

router.get('/orders/all', auth.verify, userController.getAllOrders)

// Routes for Retrieving all Orders of single user (ADMIN ONLY) (Other Features)

router.get('/orders/:id', auth.verify, userController.getUserOrders)


module.exports = router;