const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const auth = require('../auth');



// ADD TO CART (Adding products to existing order/cart)

module.exports.addProductToCart = async (req, res) => {
    try {
        let userData = await User.findById(auth.decode(req.headers.authorization).id);
        let cart = await Order.findOne({userId: auth.decode(req.headers.authorization).id})
        let productData = await Product.findOne({_id:req.body.productId});

            if (cart) {
                
                let productIndex = cart.products.findIndex(cartOrder => cartOrder.productId == productData.id);
                console.log(productIndex);
        
                if (productIndex > -1) {
                    
                    return res.status(200).send({message: "Product already exists in the cart!"});
                } else {
                    cart.products.push({
                        productId: req.body.productId,
                        name: productData.name,
                        quantity: req.body.quantity,
                        price: productData.price,
                        subtotal: req.body.quantity * productData.price
                    });
                };
                cart.totalAmount += req.body.quantity * productData.price;
                cart = await cart.save();
                return res.status(201).send({message: "Add to Bag!", result: cart});

            } else {
                const order = await Order.create ({
                    userId: userData.id,
                    products: [{
                        productId: req.body.productId,
                        name: productData.name,
                        description: productData.description,
                        quantity: req.body.quantity,
                        price: productData.price,
                        subtotal: req.body.quantity * productData.price
                    }],
                    totalAmount: req.body.quantity * productData.price
                });

                return res.status(201).send({message: "Add to Bag!", result: cart});
                
            }
        
    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};


// ADD TO CART (Updating Product Quantity)

module.exports.updateQuantity = async (req, res) => {
    try {
        let cart = await Order.findOne({userId: auth.decode(req.headers.authorization).id})
        let productData = await Product.findOne({_id:req.body.productId});

        if (cart) {
            let productIndex = cart.products.findIndex(cartProduct => cartProduct.productId == productData.id);

             if (productIndex > -1) {

                cart.products[productIndex].quantity = req.body.quantity;
                cart.products[productIndex].subtotal = req.body.quantity * productData.price;
            }

            cart.totalAmount = cart.products.reduce((previousTotal, currentValue) => previousTotal + currentValue.quantity * currentValue.price, 0);

            cart = await cart.save();
            return res.status(201).send(cart);

        };

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};


// ADD TO CART (Removing Product from Cart)

module.exports.deleteProduct = async (req, res) => {
    try {
        let cart = await Order.findOne({userId: auth.decode(req.headers.authorization).id})
        let productData = await Product.findOne({_id:req.body.productId});

        if (cart) {
            let productIndex = cart.products.findIndex(cartProduct => cartProduct.productId == productData.id);

            if (productIndex > -1) {
                cart.products.splice(productIndex, 1);
            };
            cart.totalAmount -= req.body.quantity * productData.price;
            cart = await cart.save();
            return res.status(201).send({message: "Product has been deleted!", result: cart});

        };

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};


// CheckOut (Saving the orders to the orders in product model)

module.exports.checkout = async (req, res) => {
    try {
        let cart = await Order.findOne({userId: auth.decode(req.headers.authorization).id})
        let productData = await Product.findOne({_id:req.body.productId})

        if (cart) {

            productData.orders.push({orderId: cart._id, quantity: req.body.quantity});

            productData = await productData.save();
            console.log(productData);

            return res.status(201).send({message: "Order has been placed!", result: cart});
        }
        
    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    }
}



