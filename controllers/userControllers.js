const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// User Registration

module.exports.registerUser = async (req, res) => {
    try {
        const newUser = new User ({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 10),
            mobileNo: req.body.mobileNo
        });
        
        if (!req.body.firstName || !req.body.lastName || !req.body.email || !req.body.password || !req.body.mobileNo) {

            return res.status(200).send("Please enter all required field.")
        }

        const savedUser = await newUser.save();
        console.log(savedUser);
        return res.status(201).send({message : "You have successfully signed up.", result: savedUser});

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`); 
    };
};

// User Authentication

module.exports.loginUser = async (req,res) => {
    try {
        let userData = await User.findOne({ email: req.body.email});

        if (!userData) {

            return res.status(400).send({message: "The email or password you've entered is incorrect."});
        }

        const isPasswordMatch = await bcrypt.compare(req.body.password, userData.password);

        return !isPasswordMatch ? res.status(400).send({message: "The email or password you've entered is incorrect."}) : res.status(201).send({access: auth.createAccessToken(userData)});

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);    
    };
};


// STRETCH GOAL : 

// Set user as an admin (ADMIN ONLY)

module.exports.changeUserStatus = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message: "Authorization denied!"});
        }

        let userData = await User.findByIdAndUpdate(req.params.id, {isAdmin: true});

        return res.status(200).send(userData);
        // return userData.isAdmin ? res.status(200).send({message: `${userData.firstName} ${userData.lastName} is already an Admin!`}) : res.status(201).send({message: `${userData.firstName} ${userData.lastName} is now an Admin!`});
        
    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};

// Set admin as user (ADMIN ONLY) (Other Feature)

module.exports.changeAdminStatus = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message: "Authorization denied!"});
        }

        let userData = await User.findByIdAndUpdate(req.params.id, {isAdmin: false});

         return res.status(200).send(userData);
        // return !userData.isAdmin ? res.status(200).send({message: `${userData.firstName} ${userData.lastName} is already a User!`}) : res.status(201).send({message: `${userData.firstName} ${userData.lastName} is now a User!`});

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};


// Retrieving User Details

module.exports.getDetail = async (req, res) => {
    try {
        let userData = await User.findById(auth.decode(req.headers.authorization).id)

        userData.password = "****";

        return res.status(200).send(userData);

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};

// Retrieving All User Details (ADMIN ONLY)

module.exports.getAllUsers = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message: "Authorization denied!"});
        }

        let userData = await User.find();

        return res.status(200).send(userData);

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
}


// Creating an Order / Cart

module.exports.createOrders = async (req, res) => {
    let userData = await User.findById(auth.decode(req.headers.authorization).id);
    let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});
    let productData = await Product.findOne({_id:req.body.productId});

    try { 

        if (adminData.isAdmin) {
            return res.status(401).send({message: "Authorization denied!"});
        };

        if (userData && productData) {

            const order = new Order ({
                userId: userData.id,
                products: [{
                    productId: req.body.productId,
                    name: productData.name,
                    description: productData.description,
                    quantity: req.body.quantity,
                    price: productData.price,
                    subtotal: req.body.quantity * productData.price
                }],
                totalAmount: req.body.quantity * productData.price
            });

            const savedOrder = await order.save();

            productData.orders.push({orderId: savedOrder._id, quantity: req.body.quantity}); 

            const savedProductOrder = await productData.save();

            return res.status(201).send({message: "Thank You!", result: savedOrder});
        };
        

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };

};


// Retrieving Authenticated Users Orders

module.exports.getOrders = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});
        let userOrder = await Order.find({userId: auth.decode(req.headers.authorization).id});
        // let productData = await Product.findOne({_id:req.body.productId});

        // Put the productData since if the user checkout, the product data in orders will appear to the product model ( orderId and quantity)

        if (adminData.isAdmin) {
            return res.status(401).send({message: "Authorization denied!"});
        }

        // return userOrder && productData.orders ? res.status(200).send(userOrder) : res.status(200).send({message: "You don' have any Orders"});

        // return res.status(200).send({message: "You don' have any Orders"});
        return res.status(200).send(userOrder);
        
    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};


// Retrieving all orders (ADMIN ONLY)

module.exports.getAllOrders = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message: "Authorization denied!"});
        }

        let allOrders = await Order.find({});
        return res.status(200).send(allOrders);

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};


// Retrieving all orders of single user (ADMIN ONLY) (Other Feature)

module.exports.getUserOrders = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message: "Authorization denied!"});
        }

        let userOrders = await Order.find({userId: req.params.id});

        if (!userOrders) {
            return res.status(200).send({message: "The user has no orders"})
        } 
        // let allOrders = await Order.find({});
        return res.status(200).send(userOrders);

        
    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);
    };
};


