const Product = require('../models/Product');
const User = require('../models/User');
const auth = require('../auth');


// creating a product (ADMIN ONLY).

module.exports.addProduct = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message:"Authorization denied!"});
        }

        const product = new Product(req.body);

        if (!req.body.name || !req.body.description || !req.body.price) {
            return res.status(400).send({message:"Please enter all required field."})
        }

        const savedProduct = await product.save();
        console.log(savedProduct);
        return res.status(201).send(savedProduct);

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`); 
    };
};

// Retrieving all products

module.exports.getAllProduct = async (req, res) => {
    try {
        const allProduct = await Product.find({});
        return res.status(200).send(allProduct);

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`);  
    }
};


// Retrieving all active products

module.exports.getActiveProduct = async (req, res) => {
    try {
        const activeProduct = await Product.find({isActive: true});
        return res.status(200).send(activeProduct);

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`); 
    }
};

// Retrieving single product 

module.exports.getProduct = async (req, res) => {
    try {
        const singleProduct = await Product.findById(req.params.id);
        
        return singleProduct.isActive ? res.status(201).send(singleProduct) : res.status(200).send(singleProduct);

    } catch (err) {
           console.log(err);
           return res.status(500).send(`Server Error: ${err.message}.`); 
       };
};

// Updating products details (ADMIN ONLY).

module.exports.updateProduct = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message:"Authorization denied!"});
        }

        const updateProduct = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
        };

        const updatedProduct = await Product.findByIdAndUpdate(req.params.id, updateProduct);
        return res.status(201).send({message: "The Product has been updated!"});

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`); 
    };
};

// Archiving products(ADMIN ONLY).

module.exports.archiveProduct = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message:"Authorization denied!"});
        }

        const productArchive = await Product.findByIdAndUpdate(req.params.id, {isActive: false});

        return productArchive.isActive ? res.status(201).send({message:"The product has been archived!"}) : res.status(200).send({message:"Product has already been archived!"});

    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`); 
    };
};

// Unarchive products(ADMIN ONLY) (Other Feature)

module.exports.unarchiveProduct = async (req, res) => {
    try {
        let adminData = await User.findOne({isAdmin: auth.decode(req.headers.authorization).isAdmin});

        if (!adminData.isAdmin) {
            return res.status(401).send({message:"Authorization denied!"});
        }

        const productUnarchive = await Product.findByIdAndUpdate(req.params.id, {isActive: true});

        return productUnarchive.isActive ? res.status(200).send({message:"Product is already Available!"}) : res.status(201).send({message:"The product is now Available!"});
        
    } catch (err) {
        console.log(err);
        return res.status(500).send(`Server Error: ${err.message}.`); 
    };
};
