const jwt=  require('jsonwebtoken');
const secret = "";


// Creating an access token for the Users/Admin.
module.exports.createAccessToken = (user) => {
    const data = {
        id : user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data,secret, {});

};

// Token Verification
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data) => {
			if (err) {
                return res.status(400).send({message: "Authentication Failed!"});
			} else {
                next();
			};
		});
	} else {
        return  res.status(401).send({message: "Authorization Denied!"});
	};
};

// Token decryption
module.exports.decode = (token) => {
    if (typeof token !== "undefined") {
        token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
            if (err) {
                return res.status(400).send({message: "Authentication Failed!"});
			} else {
                return jwt.decode(token, {complete:true}).payload;
			};
		});
        
	} else {
        return res.status(401).send({message: "Authorization Denied!"});
	};
};
