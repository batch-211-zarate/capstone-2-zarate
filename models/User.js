const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Please enter a valid email address"]
	},
	password: {
		type: String,
		required: [true, "Please enter a password"]
	},
	mobileNo: {
		type: String,
		required: [true, "Please enter a mobile number"]
	},
	isAdmin: {
		type: Boolean,
		default: false,
	}
},

{timestamps: true}

);

module.exports = mongoose.model("User", userSchema);