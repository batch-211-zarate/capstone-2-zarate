const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
    isActive: {
		type: Boolean,
		default: true
	},
	orders : [
		{
			orderId: {
				type: String,
				required: true
			},
			quantity: {
				type: Number,
				required: true
			}
		}
	],
},

{timestamps: true}

);

module.exports = mongoose.model("Product", productSchema);