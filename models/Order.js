const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

    userId: {
        type: String,
        required: true
    },
    products: [{
        productId: {
            type: String,
            required: true
        },
        name: {
            type: String,
        },
        description: {
            type: String,
        },
        quantity: {
            type: Number,
            required: true
        },
        price: {
            type: Number
        },
        subtotal: {
            type: Number,
            required: true
        }
    }],
    totalAmount: {
        type: Number,
        required: true
    }
},

{timestamps: true}

);

module.exports = mongoose.model("Order", orderSchema);