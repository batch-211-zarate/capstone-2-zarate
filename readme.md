# Capstone 2 - E-Commerce API

## Tabele of Contents

-	[Overview](#overview)
	-	[The challenge](#the-challenge)
	-	[The requirements](#the-requirements)
		-	[Minimum requirements](#minimum-requirements)
		-	[Other Features](#other-features)
		-	[User Credentials](#user-credentials)
		-	[Admin Credentials](#admin-credentials)
- [My process](#my-process)
  - [Built with](#built-with)


## Overview

### The challenge

The challenge is to build or develop own models for an E-commerce API with a minimum requirements and ensure that the codes are functioning.

As long as the minimum requirements are satisfied, you can add any functionality you want.


### The Requirements

#### Minimum requirements

- User Registration
- User Authentication
- Set user as admin (Admin only)
- Retrieve all products
- Retrieve all active products
- Retrieve single product
- Create Product (Admin only)
- Update Product information (Admin only)
- Archive Product (Admin only)
- Make Product Available (Admin only)
- Non-admin User checkout (Create Order)
- Retrieve User Details

#### Other Features

- Set Admin as a User
- Retrieve authenticated user’s orders
- Retrieve all orders (Admin only)
- Retrieving all Orders of single user
- Add to Cart
	- Add Products
	- Change product quantities
	- Remove products from cart
	- Subtotal for each item
	- Total price for all items

#### User Credentials

	- email: "user@mail.com"
	- password: "user1234"

#### Admin Credentials

	- email: "admin@mail.com"
	- password: "admin1234"
	

## My process

### Built with

- MongoDB
- Express.JS
- Node.js
- Postman
- Javascript


